#!/bin/bash -e
##deletes any working tree files and pulls latest
##version of files down from bitbucket repository
##only works if run as user deploy

cd [FOLDER]
git reset --hard HEAD 2>&1
git pull origin master 2>&1
chmod -R og-rx .git